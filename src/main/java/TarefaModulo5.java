import java.util.Scanner;

public class TarefaModulo5 {

    public static void main(String[] args) {
       Scanner scanner = new Scanner(System.in);
       System.out.print("Escreva um número: ");
       String numero = scanner.nextLine();

        System.out.print("Escreva outro número: ");
        String numero2 = scanner.nextLine();

        String resultadoString = numero+numero2;

        Integer resultadoInteger = Integer.parseInt(numero)+Integer.parseInt(numero2);

        System.out.println("Resultado da soma dos dois valores digitados como String: "+ resultadoString);

        System.out.println("Resultado da soma dos dois valores digitados após convertê-los em Integer: "
                + resultadoInteger);

    }
}
